#!/usr/bin/python3
import json
import os
import pymongo

HOST_NAME = "localhost"
PORT_NUM = '27017'
HOST = os.getenv('MONGO_HOST', HOST_NAME)
PORT = int(os.getenv('MONGO_PORT', PORT_NUM))

wkrole_name_map = {
    "PO": "Basic-PO",
    "SPO": "Senior-PO",
    "MPO": "Master-PO",
    "SM": "Scrum-Master",
    "SSM": "Senior-Scrum-Master",
    "MSM": "Master-Scrum-Master",
    "CCD": "Basic-Dev",
    "SCCD-L": "Senior-Dev-Linux",
    "SCCD-W": "Senior-Dev-Windows",
    "MCCD": "Master-Cyber-Capability-Dev",
    "VR": "Vulnerability-Research",
    "Mobile": "Mobile",
    "Networking": "Networking",
    "Sysadmin": "Sysadmin",
    "TAE": "Test-Automation-Engineer",
    "SEE": "Basic-SEE",
    "Instructor": "Instructor"
}


def get_all_requirements(db_obj: pymongo.database.Database) -> dict:
    '''
    Purpose: This gets a dictionary of KSAT ID to KSAT properties for each KSAT in the MTTL using Mongo DB.
    :param db_obj: The PyMongo Database object
    :return: ksats - A dictionary of each KSAT in the MTTL that is the KSAT ID to its properties
    '''
    ksats = {}
    reqs = db_obj.requirements
    # Get all KSAT info in a KSAT ID: data dictionary
    for item in reqs.find():
        ksats[item["_id"]] = item
    return ksats


def get_wkrole_ksat_prof_maps(role: str, db_obj: pymongo.database.Database) -> dict:
    '''
    Purpose: This gets a dictionary of KSAT ID to the work-role's proficiency for each KSAT in the work-role.
    :param role: The name of the work-role
    :param db_obj: The PyMongo Database object
    :return: ksat_prof_map - A dictionary of a specified work-role's KSAT to proficiency map
    '''
    ksat_prof_map = {}
    wkrls = db_obj.work_roles
    # Get all role specific KSAT ID: proficiency mappings
    for item in wkrls.find({"work-role": role}):
        if "proficiency" in item:
            ksat_prof_map[item["ksat_id"]] = item["proficiency"]
        else:
            ksat_prof_map[item["_id"]] = ""
    return ksat_prof_map


def get_all_wkrole_ksat_prof_maps(mttl_dir: str, db_obj: pymongo.database.Database) -> dict:
    '''
    Purpose: This gets a dictionary of all work-roles in the MTTL that map to a dictionary of all their KSAT ID to
    proficiency maps. The structure is as follows: {<work-role>: {<'[K|S|A|T] ID'>: <proficiency>, ...}, ...}.
    :param mttl_dir: The path to the MTTL repository; keep in mind, it can be a relative path
    :param db_obj: The PyMongo Database object
    :return: all_ksat_prof_maps - A dictionary of all work-roles to their respective KSAT ID to proficiency maps.
             {<work-role>: {<'[K|S|A|T] ID'>: <proficiency>, ...}, ...}
    '''
    all_ksat_prof_maps = {}
    # Get all role specific KSAT IDs in a list
    # Using old method due to a more complete list being returned
    for role in get_rdmap_names(mttl_dir):
        all_ksat_prof_maps[role.upper()] = get_wkrole_ksat_prof_maps(role, db_obj)
    return all_ksat_prof_maps


def get_only_mapped_wkrole_ksat_prof_maps(db_obj: pymongo.database.Database) -> dict:
    '''
    Purpose: This gets a dictionary of work-roles (only those with mapped KSATs) in the MTTL that map to a dictionary of
    all their KSAT ID to proficiency maps.
    The structure is as follows: {<work-role>: {<'[K|S|A|T] ID'>: <proficiency>, ...}, ...}.
    :param db_obj: The PyMongo Database object
    :return: all_ksat_prof_maps - A dictionary of work-roles (only those with mapped KSATs) to their respective KSAT ID
             to proficiency maps. {<work-role>: {<'[K|S|A|T] ID'>: <proficiency>, ...}, ...}
    '''
    mapped_ksat_prof_maps = {}
    # Get all role specific KSAT IDs in a list
    for role in get_mapped_wkrole_names(db_obj):
        mapped_ksat_prof_maps[role.upper()] = get_wkrole_ksat_prof_maps(role, db_obj)
    return mapped_ksat_prof_maps


def get_mapped_wkrole_names(db_obj: pymongo.database.Database) -> list:
    '''
    Purpose: This gets a list of all work-roles in the MTTL that have KSATs mapped.
    :param db_obj: The PyMongo Database object
    :return: A list representing the work-roles with mapped KSATs in the MTTL.
    '''
    wkrls = db_obj.work_roles
    # Get all work-role names in a list
    return list(wkrls.find().distinct("work-role"))


def get_rdmap_names(mttl_path: str, rdmap_fpath: str = "Roadmap.json") -> list:
    '''
    Purpose: Generate a list of Roadmap names according to the current MTTL.
    :param mttl_path: The path to the MTTL repository; keep in mind, it can be a relative path
    :param rdmap_fpath: The file name, including necessary path(s) relative to mttl_path, of the Roadmap JSON file;
                          the default is 'Roadmap.jsonn'
    :return: Upon success: roles = list of Roadmap names
             Upon failure:
                raise ValueError = an empty list of work role or specialization files is received, or a work role or
                    specialization file doesn't exist
                raise json.JSONDecodeError, FileNotFoundError, FileExistsError, OSError, or IOError = one or more
                    work role/specialization files cannot be loaded into a dictionary
                [] = Data Error
    '''
    roles = []

    try:
        with open(os.path.join(mttl_path, rdmap_fpath), "r") as rdmap_fp:
            for role, data in json.load(rdmap_fp).items():
                if role == "Special Missions" or role == "MPO" or role == "MSM" or role == "MCCD":
                    if role != "Special Missions" and role not in roles:
                        roles.append(role)
                    for course in data["courses"]:
                        if course["name"] not in roles:
                            roles.append(course["name"])
                else:
                    roles.append(role)

    except (json.JSONDecodeError, FileNotFoundError, FileExistsError, OSError, IOError, ValueError):
        raise

    return roles


def get_module_lsn_data(module: str, db_obj: pymongo.database.Database) -> list:
    '''
    Purpose: This retrieves all data with the specified module name in ascending topic order.
    :param module: The name of the module
    :param db_obj: The PyMongo Database object
    :return: A list of all Module name objects in topic order
    '''
    rel_links = db_obj.rel_links
    return list(rel_links.find({"module": module}).sort("topic", 1))


def get_module_topics(module: str, db_obj: pymongo.database.Database) -> list:
    '''
    Purpose: This retrieves a list of topics with the specified module name in ascending topic order.
    :param module: The name of the module
    :param db_obj: The PyMongo Database object
    :return: A list of all topics in the specified Module in ascending order
    '''
    rel_links = db_obj.rel_links
    return list(rel_links.find({"module": module}).sort("topic", 1).distinct("topic"))


def get_module_subjects(module: str, db_obj: pymongo.database.Database) -> list:
    '''
    Purpose: This retrieves a list of subjects with the specified module name in ascending subject order.
    :param module: The name of the module
    :param db_obj: The PyMongo Database object
    :return: A list of all subjects in the specified Module in ascending order
    '''
    rel_links = db_obj.rel_links
    return list(rel_links.find({"module": module}).sort("subject", 1).distinct("subject"))


def get_single_requirement(oid: str, db_obj: pymongo.database.Database) -> dict:
    '''
    Purpose: This retrieves the requirement data for the given requirement's MTTL object ID.
    :param oid: The object ID as stored in the MTTL
    :param db_obj: The PyMongo Database object
    :return: A single requirement's info
    '''
    requirements = db_obj.requirements
    return requirements.find_one({"_id": pymongo.collection.ObjectId(oid)})


def get_question(oid: str, db_obj: pymongo.database.Database) -> dict:
    '''
    Purpose: This retrieves the rel-link data for the given question's MTTL object ID.
    :param oid: The object ID as stored in the MTTL
    :param db_obj: The PyMongo Database object
    :return: A single question's rel-link info
    '''
    rel_links = db_obj.rel_links
    return rel_links.find_one({"_id": pymongo.collection.ObjectId(oid)})


def get_question_ksats(oid: str, db_obj: pymongo.database.Database) -> list:
    '''
    Purpose: This retrieves a list of KSAT IDs that's mapped for the given question's MTTL object ID.
    :param oid: The object ID as stored in the MTTL
    :param db_obj: The PyMongo Database object
    :return: A list of all KSAT IDs mapped to the question
    '''
    rel_links = db_obj.rel_links
    ksats = []
    for ksat_oid in list(rel_links.find({"_id": pymongo.collection.ObjectId(oid)}).distinct("KSATs.ksat_id")):
        ksats.append(get_single_requirement(ksat_oid, db_obj)["ksat_id"])
    return ksats


def get_question_workroles(oid: str, db_obj: pymongo.database.Database) -> list:
    '''
    Purpose: This retrieves a list of KSAT IDs that's mapped for the given question's MTTL object ID.
    :param oid: The object ID as stored in the MTTL
    :param db_obj: The PyMongo Database object
    :return: A list of all work-roles mapped to the question
    '''
    rel_links = db_obj.rel_links
    return list(rel_links.find({"_id": pymongo.collection.ObjectId(oid)}).distinct("work-roles"))
