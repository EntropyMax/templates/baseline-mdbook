from colorama import Fore
import enum


class ErrorCode(enum.Enum):
    SUCCESS = 0
    EMPTY_LIST = 1
    JSON_DECODE_ERROR = 2
    INVALID_JSON_FORMAT = 3
    INVALID_KSAT_MAPPING = 4
    UNABLE_TO_BUILD_KSAT_LIST = 5
    INVALID_INPUT = 6
    UNABLE_TO_GEN_UID = 7
    UNABLE_TO_GEN_IMAGE = 8
    UNABLE_TO_GEN_MARKDOWN = 9
    INVALID_SLIDE = 10
    DUPLICATE_QUESTION_IDS = 11


class Errors:
    def __init__(self, error: ErrorCode = ErrorCode.SUCCESS):
        self.error = error
        self.custom_msg = ""

    def get_msg(self) -> str:
        return self.error.name

    def get_code(self) -> int:
        return self.error.value

    def get_full_error_msg(self) -> str:
        msg = Fore.CYAN + self.get_msg() + Fore.RESET
        if "" != self.custom_msg:
            custom_msg = ": {0}".format(self.custom_msg)
        else:
            custom_msg = ""
        if self.get_code() == 0:
            code = Fore.GREEN + str(self.get_code()) + Fore.RESET
        else:
            code = Fore.RED + str(self.get_code()) + Fore.RESET
        return "({0}): {1}{2}".format(code, msg, custom_msg)

    def set_error(self, error: ErrorCode):
        self.error = error


if __name__ == "__main__":
    err = Errors()
    # Test default settings
    print("{0}: {1}".format(err.get_code(), err.get_msg()))
    print(err.get_full_error_msg())
    # Test setting error
    err.set_error(ErrorCode.INVALID_INPUT)
    print("{0}: {1}".format(err.get_code(), err.get_msg()))
    print(err.get_full_error_msg())
